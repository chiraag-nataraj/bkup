# bkup

`bkup` is a script that can be used to generate encrypted tar archives compressed with `zstd` and split into chunks (default is 50MB, but can be customized).

## Motivation

I recently started using [Backblaze B2](https://www.backblaze.com/b2/cloud-storage.html) as my offsite backup system. However, unlike their personal backup service, they do not client-side encrypt the data they store (which makes sense - while they have an official CLI client, the real power comes from their APIs and integrations with other tools). While there are other fantastic tools out there, such as [duplicity](https://duplicity.nongnu.org), [restic](https://restic.org), and [rclone](https://rclone.org)'s `crypt` backend, they all lacked some feature or the other which I wanted.

* GPG for encryption - this one was necessary as I already use a lot of other GPG-based tools and don't want to manage another keypair
* Fully recoverable from the cloud - I should be able to reconstruct everything (including filenames) from whatever I back up to the cloud
* Nothing should be exposed to the server - filenames, type of data - all of it should be opaque to the backup server

Because of these reasons, I decided to roll my own script/program for incremental, encrypted, compressed backups.

## Design

The first time you run `bkup` on a directory (`${HOME}/$dir`, where `$dir` is the _relative_ path from `$HOME`), it will generate a unique salt and store it in `${XDG_DATA_DIR}/bkup/$dir/.salt.gpg`. At the same time, it will also write the value of `$dir` to `${XDG_DATA_DIR}/bkup/$dir/.name.gpg`. Using the value of `$dir` and `$salt`, it will generate the SHA1 hash `$hash` (originally, I was storing each folder in a separate bucket on B2, and the length of a SHA256 hash got too long for a bucket name, so I switched to SHA1). It also hard-links these files to `${XDG_CACHE_DIR}/bkup/$dir/`. At this point, it also records the current date/time to use later.

It then finds all files newer than epoch (because it's the first run) which, in most cases, should be sufficient to catch all files (if there are issues with this, please open a bug report and I'll fix it). It passes this file list to `tar`, which pipes it to `zstd` (with the highest compression setting), which then pipes it to `split`, which creates 50MB (by default) chunks and passes each one off to `gpg` to be encrypted. These encrypted chunks are written to `${XDG_CACHE_DIR}/bkup/$dir/$hash-$year-$month-$day-$hour-$minute-$second.tar.zst.nnnnnn.gpg` (the suffix length is 6 by default, but can be changed by editing the script) - note that the unencrypted chunks are never written to disk.

As a final step, `bkup` writes the timestamp of the run to `${XDG_DATA_DIR}/bkup/$dir/.date.gpg` and hard-links that file to `${XDG_CACHE_DIR}/bkup/$dir/`. The next time `bkup` is run, it will read in that date and only look for newer files, thus generating incremental archives with infinite versions (should you choose to do so). To regenerate a full backup, simply delete `.date.gpg` and `bkup` will run a full backup the next time you ask it to back up that directory.

This means that one can now use `ssh`/`ftp`/`rclone`/whatever to reliably transmit these chunks to the remote server for backup. Copying everything in `${XDG_CACHE_DIR}/bkup/$dir` should be sufficient.

This design has a couple of ramifications:

* In order to reconstruct the hashes, one simply has to read in `.name.gpg` and `.salt.gpg` and pass them to `sha1sum`. Since those files are backed up, it means that one can always regenerate the hashes, thus leading to an easy way to, say, automatically select the folder to upload files to.
* On the flip side, if you are trying to restore your data and either have a slow connection or pay per API call, transaction, or GB downloaded, you can download all the `.name.gpg` files to figure out which hashes correspond with which directories, which means you can prioritize downloading the data you need first.
* *Always* have a backup of your GPG key, and *always* encrypt to more than one key. You never want to be in a position where you have your backup but you can't access it! Something like a [Yubikey](https://yubico.com) might be helpful for this purpose.
* If you ever lose your local copy of `${XDG_DATA_DIR}/bkup`, you can always restore by downloading `.name.gpg`, `.salt.gpg`, and `.date.gpg` from each folder/bucket/whatever you've backed up to, reading `.name.gpg`, and moving the files to the appropriate place within `${XDG_DATA_DIR}/bkup/`, allowing you to seamlessly pick up where you left off.
* By splitting up files explicitly, it's easy to parallelize uploads and downloads using `rclone`, and stopping in the middle doesn't cancel the entire file transfer.

## Usage

The syntax is pretty simple. The positional argument is the directory to back up and everything else is passed as optional arguments. An example might be: `bkup -k 'subkey1!' -k 'subkey2!' .config`. The final output would be:

* `~/.local/share/bkup/.config/.name.gpg`, `~/.local/share/bkup/.config/.salt.gpg`, `~/.local/share/bkup/.config/.date.gpg`
* `~/.cache/bkup/.config/.name.gpg`, `~/.cache/bkup/.config/.salt.gpg`, `~/.cache/bkup/.config/.date.gpg` (hard-links to the files in `~/.local/share/bkup/.config/`)
* `~/.cache/bkup/.config/<sha1sum>-<date>.tar.zst.nnnnnn.gpg`
* All the above files are encrypted to both `subkey1` and `subkey2` and can be decrypted with either.

## Options

Here are all of the supported options:

* `-c`, `--chunk-size`: Change the chunk size (default: 50M)
* `-C`, `--cleanup`: run rclone's cleanup operation after uploading (default: off)
* `--compress-option`:  Pass option to compress program (default: `-T0` and `-19` are passed if this option is not specified)
* `--compress-suffix`:  Set suffix of compressed file (default: `zst`)
* `--delete`: delete local files (in `${XDG_CACHE_DIR}/bkup/$dir`) after successful upload (default: off)
* `--gpg-option`: Pass  additional option to `gpg` (default: `--no-encrypt-to`, `--yes`, `--quiet`, and `--compress-algo=none` are passed by default)
* `-h`, `--help`: Print out help text
* `-k`, `--encrypt-key`: Specify the key or subkey to encrypt to (`bkup` will error out if no keys are specified) (default: none)
* `--no-posix`: Use GNU `tar` format instead of POSIX `tar` format (POSIX `tar` has higher time resolution but is supposedly less well-supported...given that GNU `tar` is pretty much universally available, this shouldn't be an issue)
* `--rclone-mode`: Specify the rclone mode (default: copy) (copy and sync are supported)
* `--rclone-option`: Pass additional option to `rclone` (default: `-P` is always passed by default, add `-q` to silence)
* `-s`, `--sign-key`: Specify the key or subkey to sign with (default: none)
* `--suffix-length`: Change the suffix length (default: 6)
* `-u`, `--upload`: Specify the remote to upload to (default: none)
* `-z`, `--compress-program`:  Set compress program (default: `zstd`)

## Notes

The `--compress-` options are provided mainly for the case where `zstd` may take too much memory (or is not available). See the program's help for more information how to use them.

## Unattended Usage

One annoying thing about this design is that the salt and date files need to be decrypted interactively. In order to enable unattended usage, `bkup` will first look for decrypted versions (`.salt` and `.date` respectively) and will automatically update the decrypted `.date` file if it finds one. **You must write the decrypted files yourself** — `bkup` will not do that for you.

To reiterate, **under no circumstance** will `bkup` write these decrypted files if they don't exist already. Nor will it ever copy these files into `${XDG_CACHE_DIR}`.

The easiest way to have unattended _signed_ backups is to generate a separate key with no passphrase and use it for signing. This is what I have done and it works wonderfully.
